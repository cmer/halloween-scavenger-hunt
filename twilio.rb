require 'sinatra'
require './config'

post '/message' do
  game = Game.new
  number = params['From']
  cmd = params['Body'].downcase.strip

  if body != ''
    response = game.handle_command(cmd)
    output_msg = "<Response>"

    response.each do |str|
      output_msg += "<Message>#{str}</Message>"
    end

    content_type 'text/xml'
    output_msg += "</Response>"
    output_msg
  end
end

get '/' do
  "Hello!!!"
end