require 'airrecord'

Airrecord.api_key = ENV['AIRTABLE_KEY']

class Questions < Airrecord::Table
  self.base_key = ENV['AIRTABLE_APPS']
  self.table_name = "Questions"

  def self.reset_game!
    self.all.each do |q|
      if q["Answered Correctly"]
        q["Answered Correctly"] = false
        q.save
      end
    end
  end

  def self.current_question
    self.all(filter: "NOT({Answered Correctly})", sort: {'ID' => 'asc'}).first
  end

  def self.validate_answer!(answer)
    cur_q = self.current_question

    return nil unless cur_q

    if cur_q["Answer"].downcase.strip == answer.downcase.strip
      cur_q["Answered Correctly"] = true
      cur_q.save
      true
    else
      false
    end
  end
end