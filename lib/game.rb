class Game
  DISSES = [
    "Nope. That's wrong!",
    "You're really horrible at this.",
    "Really!?!?!?!?!?!",
    "Try a bit harder",
    "NOPE!",
    "NAH!!!!",
    "LOLZ",
    "Even Seniac would get this right!",
    "BRUH!!!!!!!!!!!!!!!!!!!!!!!!!!!!",
    "Is your brain on vacation or what?",
    "Can't believe you can't figure this out...",
    "AHAHHAHAHAHAH you'll never guess!!!!!!!!!!!!!!!!!!!!"
  ]

  PRAISES = [
    "Woah that was awesome!!!",
    "Excellent, little boy!",
    "YOU RULE.",
    "YAYYYYY!",
    "Correct!",
    "Look at that big brain!",
    "Woah!!!!!!!!",
    "Very nice.",
    "Excellent.",
    "Goooooooooood!!!",
    "YEP!",
    "Good work!",
    "EPIC",
    "Phenomenal",
    "Perfect"
  ]

  def handle_command(cmd)
    cmd = cmd.strip.downcase
    output = []

    case cmd
    when 'start', 'restart', 'reset'
      Questions.reset_game!
      output << print_welcome
      output << print_current_question
    when 'quit'
      output << "bye bye!"
    when 'repeat'
      output << print_current_question
    else
      is_valid = Questions.validate_answer!(cmd)
      if is_valid == true
        output << print_praise
        output << print_current_question
      elsif is_valid == false
        output << print_diss
      else # nil
        output << how_to_restart
      end
    end
  end

  def print_diss
    DISSES.sample
  end

  def print_praise
    PRAISES.sample
  end

  def print_game_finished
    "Congratulations, you've won the game! #{print_how_to_restart}"
  end

  def print_how_to_restart
    "To start over, simply type: RESTART"
  end

  def print_question(question)
    id = question['ID']
    "Question ##{id}: #{question['Question']}"
  end

  def print_current_question
    cur_q = Questions.current_question
    if cur_q
      print_question(cur_q)
    else
      print_game_finished
    end
  end

  def print_welcome
    "Welcome. Are you brave enough to pay a little game on this spooky Halloween night? Watch out, you never know when zombies will get hungry!"
  end
end