require './config'

puts "\e[H\e[2J" # clear screen

game = Game.new
cmd  = nil

begin
  print '⇨ '
  cmd = gets.chomp.to_s.downcase.strip

  if cmd != ""
    output = game.handle_command(cmd)

    output.each do |str|
      puts str + "\n"
    end
  end
end while cmd != 'quit'
